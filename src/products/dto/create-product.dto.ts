import { IsNotEmpty, Length } from 'class-validator';
import { IsPositive } from 'class-validator/types/decorator/decorators';

export class CreateProductDto {
  @IsNotEmpty()
  @Length(3, 20)
  name: string;

  @IsNotEmpty()
  @IsPositive()
  price: number;
}
